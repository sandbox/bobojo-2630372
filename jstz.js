/**
 * Simple script to attach user's detected timezone to Drupal.settings
 */
(function($) {
  Drupal.behaviors.jstz = {
    attach: function (context, settings) {
      Drupal.settings.jstz = jstz.determine().name();
    }
  }
})(jQuery);
