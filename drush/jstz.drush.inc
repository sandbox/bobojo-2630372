<?php

/**
 * @file
 * drush integration for jsTimezoneDetect module.
 */

/**
 * Implments hook_drush_command().
 */
function jstz_drush_command() {
  $items = array();

  $items['jstz-download'] = array(
    'callback' => 'jstz_drush_download',
    'description' => dt("Downloads the jsTimezoneDetect library."),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'path' => dt('Optional. The path to your shared libraries. If omitted Drush will use the default location.'),
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function jstz_drush_help($section) {
  $path = 'sites/all/libraries';
  switch ($section) {
    case 'drush:jstz-download':
      $name = 'jsTimezoneDetect';
      $msg = dt("Downloads @name. Default location is @path.", array('@name' => $name, '@path' => $path));
      return $msg;
  }
}

function jstz_drush_download() {
  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  $library = libraries_info('jstz');
  $filename = basename($library['download url']);
  $dirname = 'jstz';

  if (is_dir($dirname)) {
    chdir($dirname);
    // Remove any existing jsTimezoneDetect
    if (is_file($filename)) {
      drush_log(dt('An existing jsTimezoneDetect was overwritten at @path', array('@path' => $path)), 'notice');
      drush_op('unlink', $filename);
    }
  }
  else {
    drush_op('mkdir', $dirname);
    chdir($dirname);
  }

  // Download the jsTimezoneDetect library
  if (!drush_shell_exec('wget '. $library['download url'])) {
    drush_shell_exec('curl -O '. $library['download url']);
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);

  if (is_dir($path .'/'. $dirname)) {
    drush_log(dt('jsTimezoneDetect library has been downloaded to @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download jsTimezoneDetect library to @path', array('@path' => $path)), 'error');
  }
}
